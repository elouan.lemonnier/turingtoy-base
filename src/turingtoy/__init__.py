from typing import (
    Dict,
    List,
    Optional,
    Tuple,
    Any
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)

def run_turing_machine(machine: Dict, input_: str, steps: Optional[int] = None) -> Tuple[str, List, bool]:

    blank = machine['blank']
    start_state = machine['start state']
    final_states = machine['final states']
    table = machine['table']
    
    inputList = list(input_)
    current_state = start_state
    position = 0
    history = []
    execution_history = {}
    leaving_state = 0
    
    def write_symbol(symbol):
        print("WRITE : " + symbol, position)
        
        inputList[position] = symbol
        

    def move_head(direction):
        nonlocal position
        if direction == 'L':
            position -= 1
            if position < 0:
                position = 0
                inputList.insert(0, blank)
            return direction
        elif direction == 'R':
            position += 1
            if(position == len(inputList)):
                inputList.append(blank)
            return direction
        

    def read_symbol():
        if position < 0 or position >= len(inputList):
            return blank
        return inputList[position]
    
    def transition(state, symbol):
        return table[state][symbol]
    
    def execute_step(current_state):
        symbol = read_symbol()

        execution_history["state"] = current_state
        execution_history["reading"] = symbol
        execution_history["position"] = position
        execution_history["memory"] = ''.join(inputList)
        
        transition_data = transition(current_state, symbol)

        execution_history["transition"] = transition_data

        if(transition_data == "L"):
            direction = move_head('L')
        elif(transition_data == "R"):
            direction = move_head('R')
        else:
            if "write" in transition_data:
                if write_symbol(transition_data.get('write', blank)) == 2:
                    return 2, current_state, execution_history

            if(transition_data.get('L', '') != ''):
                direction = move_head('L')
            else:
                direction = move_head('R')

            next_state = transition_data.get(direction)
            
            if transition_data.get(direction, blank) in final_states:
                return 1, current_state, execution_history
            if next_state:
                current_state = next_state
            
        
        return 0, current_state, execution_history
    
    for step in range(steps) if steps else range(1000000):
        
        
        if current_state in final_states:
            break
        execOutput = execute_step(current_state)
        
        current_state = execOutput[1]
        leaving_state = execOutput[0]
        history.append(execOutput[2].copy())
        
        if leaving_state != 0:
            if leaving_state == 1: 
                leaving_state = True
            else: 
                leaving_state = False
            break
    
    output = ''.join(inputList)
    output = output.strip(blank)
     
    return output, history, leaving_state

def to_dict(keys: List[str], value: Any) -> Dict[str, Any]:
    return {key: value for key in keys}

machine = {
        "blank": " ",
        "start state": "right",
        "final states": ["done"],
        "table": {
            # Start at the second number's rightmost digit.
            "right": {
                **to_dict(["0", "1", "+"], "R"),
                " ": {"L": "read"},
            },
            # Add each digit from right to left:
            # read the current digit of the second number,
            "read": {
                "0": {"write": "c", "L": "have0"},
                "1": {"write": "c", "L": "have1"},
                "+": {"write": " ", "L": "rewrite"},
            },
            # and add it to the next place of the first number,
            # marking the place (using O or I) as already added.
            "have0": {**to_dict(["0", "1"], "L"), "+": {"L": "add0"}},
            "have1": {**to_dict(["0", "1"], "L"), "+": {"L": "add1"}},
            "add0": {
                **to_dict(["0", " "], {"write": "O", "R": "back0"}),
                "1": {"write": "I", "R": "back0"},
                **to_dict(["O", "I"], "L"),
            },
            "add1": {
                **to_dict(["0", " "], {"write": "I", "R": "back1"}),
                "1": {"write": "O", "L": "carry"},
                **to_dict(["O", "I"], "L"),
            },
            "carry": {
                **to_dict(["0", " "], {"write": "1", "R": "back1"}),
                "1": {"write": "0", "L": "carry"},
            },
            # Then, restore the current digit, and repeat with the next digit.
            "back0": {
                **to_dict(["0", "1", "O", "I", "+"], "R"),
                "c": {"write": "0", "L": "read"},
            },
            "back1": {
                **to_dict(["0", "1", "O", "I", "+"], "R"),
                "c": {"write": "1", "L": "read"},
            },
            # Finish: rewrite place markers back to 0s and 1s.
            "rewrite": {
                "O": {"write": "0", "L": "rewrite"},
                "I": {"write": "1", "L": "rewrite"},
                **to_dict(["0", "1"], "L"),
                " ": {"R": "done"},
            },
            "done": {},
        },
    }

input_ = "11+1"



print(run_turing_machine(machine, input_))

